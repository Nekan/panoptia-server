New-UDPage -Name "about" -Content {
    New-UDHelmet -Content {
        New-UDHtmlTag -Tag 'title' -Content { "A propos de - Panoptia Online" }
    }
    New-UDRow {
        New-UDColumn -SmallSize 4 -Content {
            New-UDImage -Url 'https://www.shyrkasystem.com/dl/gitlab.png' -Height 140
        }
        New-UDColumn -SmallSize 4 -Content {
            New-UDImage -Url 'https://www.shyrkasystem.com/dl/logo-400.png' -Height 140 -Width 400
        }
        New-UDColumn -SmallSize 4 -Content {
            New-UDImage -Url 'https://aws1.discourse-cdn.com/standard11/uploads/universaldashboard/original/1X/dcbdc2b0d348501ae3b48bddf6e66ae2c7d15519.png' -Height 140
        }
    }
    New-UDRow {
        New-UDColumn -LargeSize 6 -Content {
            New-UDCard -Title "Shyrka Sysytem" -Size small -Content {
                New-UDParagraph -Text "Panoptia Online"
                New-UDParagraph -Text "Licence GPLv3"
                New-UDParagraph -Text "R�alis� par Nicolas THOREZ - Shyrka System"
            } -Links @(
                New-UDLink -Text "Site officiel" -Url 'https://www.shyrkasystem.com'
                New-UDLink -Text "Documentation" -Url 'kttps://www.shyrkasystem.com/doku.php?id=panoptia'
            )
        }
        New-UDColumn -LargeSize 6 -Content {
            New-UDCard -Title "Gitlab" -Size small -Content {
                New-UDParagraph -Text "Les sources sont disponibles sur Gilbab."
                New-UDParagraph -Text " "
                New-UDParagraph -Text " "
            } -Links @(
                New-UDLink -Text "Panoptia Online" -Url 'https://gitlab.com/Nekan/panoptia-server'
                New-UDLink -Text "Client et modules" -Url 'https://gitlab.com/Nekan/panoptia-client'
            )
        }
    }
    New-UDRow {
        New-UDCard -Title "Ironman Software" -Content {
            New-UDParagraph -Text "UniversalDashboard est un produit de la soci�t� Iromman Software."
            New-UDParagraph -Text "Un grand merci � Adam DRISCOLL pour ces travaux."
        } -Links @(
            New-UDLink -Text "Site officiel" -Url 'https://universaldashboard.io/'
            New-UDLink -Text "Documentation" -Url 'https://docs.universaldashboard.io/'
            New-UDLink -Text "Ironman Software" -Url 'https://ironmansoftware.com/'
        )
    }
}