<#
.SYNOPSIS

    ███████╗██╗  ██╗██╗   ██╗██████╗ ██╗  ██╗ █████╗                 
    ██╔════╝██║  ██║╚██╗ ██╔╝██╔══██╗██║ ██╔╝██╔══██╗                
    ███████╗███████║ ╚████╔╝ ██████╔╝█████╔╝ ███████║                
    ╚════██║██╔══██║  ╚██╔╝  ██╔══██╗██╔═██╗ ██╔══██║                
    ███████║██║  ██║   ██║   ██║  ██║██║  ██╗██║  ██║                
    ╚══════╝╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝                
                                                                  
                ███████╗██╗   ██╗███████╗████████╗███████╗███╗   ███╗
                ██╔════╝╚██╗ ██╔╝██╔════╝╚══██╔══╝██╔════╝████╗ ████║
                ███████╗ ╚████╔╝ ███████╗   ██║   █████╗  ██╔████╔██║
                ╚════██║  ╚██╔╝  ╚════██║   ██║   ██╔══╝  ██║╚██╔╝██║
                ███████║   ██║   ███████║   ██║   ███████╗██║ ╚═╝ ██║
                ╚══════╝   ╚═╝   ╚══════╝   ╚═╝   ╚══════╝╚═╝     ╚═╝ 
 
#############################################################################################################
#                                                                                                           #
# Script de démarrage du tableau de bord de supervision Panoptia                                            #
#                                                                                                           #
# Par Nicolas THOREZ                                                                                        #
#                                                                                                           #
#############################################################################################################
 
.DESCRIPTION

Ce script lance le dashboard, basé sur le framework Universal Dashboard (Community).

.PARAMETER Port
Indique le port de publication du dashboard. Par défaut, le port utilisé est le 80.

.EXAMPLE
Start-Dashboard.ps1 -Port 10001

Publie le dashboard sur le port 10001. Le site sera donc accessible à l'adresse http://localhost:10001/

.OUTPUTS
La sortie se fait sous la forme d'une publication de page web.
     
.NOTES

Sources : https://gitlab.com/Nekan/panoptia-server

Plus d'informations sur https://www.shyrkasystem.com

#>

# Gestion des paramètres d'entrée
[CmdletBinding()]
param (
    [Parameter()]
    [UInt16]
    $Port = 80
)

# Import des modules
Import-Module -Name UniversalDashboard.Community
Import-Module -Name UniversalDashboard.Helmet

# Pages
$Pages = @()
$Pages += . (Join-Path $PSScriptRoot "pages\home.ps1")
$Pages += . (Join-Path $PSScriptRoot "pages\about.ps1")

# Ajout dynamique des toutes les pages disponibles
(Get-Item -Path (Join-Path $PSScriptRoot "pages\*") -Filter *.ps1 -Exclude home.ps1,about.ps1).Name | Sort-Object | ForEach-Object {
    $Pages += . (Join-Path $PSScriptRoot "pages\$_")
}

# Menu
$NavBarContent = {
    New-UDSideNavItem -Text "Accueil" -Url "home" -Icon home
    New-UDSideNavItem -Text "A propos de..." -Url "about" -Icon question_circle

    # # Ajout dynamique des autres pages
        (Get-Item -Path (Join-Path $PSScriptRoot "pages\*") -Filter *.xml).Name | Sort-Object | ForEach-Object {
            [xml]$XmlContent = Get-Content -Path (Join-Path $PSScriptRoot "pages\$_")
            New-UDSideNavItem -Text $XmlContent.Document.Text -Url $XmlContent.Document.Url -Icon $XmlContent.Document.Icon 
        }
}
$NavBar = New-UDSideNav -Content $NavBarContent

# Thème
$Theme = New-UDTheme -Name "PanoptiaDark" -Definition @{
    # Thème pour l'entrée de menu sélectionnée
    '.sidenav a:hover' = @{
        'color' = "#7abb4d"
    }

    # Theme des entrées du menu
    '.sidenav li > a' = @{
        'font-size' = "18px"
    }
} -Parent DarkRounded

# Logo
$LogoPath = Join-Path $PSScriptRoot "..\images\panoptia-white.png"
$Logo = New-UDImage -Path $LogoPath -Height 45 -Width 45

# Création du dashbord
$Dashboard = New-UDDashboard -Title "Panoptia Online" -page $Pages -Theme $Theme -NavBarLogo $Logo -Navigation $NavBar

# Purge des dashboard déjà actifs
Get-UDDashboard | Stop-UDDashboard

# Initialisation du dashboard
Start-UDDashboard -Port $Port -Dashboard $Dashboard -Name "PanoptiaOnline"