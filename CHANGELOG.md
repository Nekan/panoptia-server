# Panoptia - Côté serveur - Suivi de version

## 0.0.0.1

- Ajout de la page principale
- Ajout du thème
- Ajout de la barre de navigation
- Mécanisme d'ajout automatique des pages contenues dans le dossier pages
- Mécanisme de construction du menu via les fichiers XML correspondant aux différentes pages
- Ajout du logo